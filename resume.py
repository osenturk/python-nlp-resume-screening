# Resume Phrase Matcher code


# importing all required libraries
import PyPDF2
from collections import Counter
import logging
import spacy
import os

basedir = os.path.abspath(os.path.dirname(__file__))

# function to read resume ends
logger = logging.getLogger(__name__)

# Load the en_core_web_sm model
nlp = spacy.load('en_core_web_sm')
# nlp = en_core_web_sm.load()
stopwords = spacy.lang.en.stop_words.STOP_WORDS

def pdfextract(pdf_path):
    with open(pdf_path, 'rb') as f:
        pdf = PyPDF2.PdfFileReader(f)


        countpage = pdf.getNumPages()
        count = 0
        text = []
        while count < countpage:
            pageObj = pdf.getPage(count)
            count += 1
            t = pageObj.extractText()
            text.append(t)

        return text


# Function to preprocess text
def lemmatize(text):

    # Create Doc object
    doc = nlp(text, disable=['ner', 'parser'])
    # Generate lemmas
    lemmas = [token.lemma_ for token in doc]
    # Remove stopwords and non-alphabetic characters
    a_lemmas = [lemma for lemma in lemmas
                if lemma.isalpha() and lemma not in stopwords]

    return ' '.join(a_lemmas)

# function that does phrase matching and builds a candidate profile
def create_profile(file):
    text_list = pdfextract(file)
    text = ''.join(text_list)
    text = lemmatize(text)

    countr = Counter(text.split())

    return countr.most_common(20)


one_list = create_profile("ozansenturk.pdf")
print(one_list)

# import PyPDF2

# # creating an object
# file = open('ozansenturk.pdf', 'rb')

# # creating a pdf reader object
# fileReader = PyPDF2.PdfFileReader(file)

# # print the number of pages in pdf file
# print(fileReader.numPages)

