Natural Language Processing tool to screen Data Science Resumes


For general queries and support, please contact Ozan Senturk
(ozan.senturk@gmail.com)

Yours truly,

Ozan Senturk

p.s. inspired by Venkat Raman by his article 'How I used NLP (Spacy) to screen Data Science Resumes'
https://towardsdatascience.com/do-the-keywords-in-your-resume-aptly-represent-what-type-of-data-scientist-you-are-59134105ba0d

